import logging
import os
import requests

from apsw import CorruptError
from eyed3.mp3 import Mp3AudioFile
from multiprocessing import Process, Queue

from . import settings
from .api import FILE_EXTENSIONS
from .models import Song

logger = logging.getLogger(__name__)

SENTINEL = 'STOP'


class DownloadError(LookupError):
    pass


def download(song_download, file_path, session=None):
    if session is None:
        logger.debug('No session was passed, creating one...')
        session = requests.session()

    # Fetch the file and write it to disk
    with open(file_path, 'wb') as f:
        f.write(session.get(song_download.url).content)

    Song.update(downloaded=True).where(Song.identity == song_download.song.identity).execute()


def handle_downloads(downloads, session):
    """
    Process the download queue.
    """
    for song_download, file_path in iter(downloads.get, SENTINEL):
        try:
            download(song_download, file_path, session=session)
            if song_download.encoding == 'mp3':
                logger.debug('File is mp3 format, adding metadata.')
                audio_file = Mp3AudioFile(file_path)
                audio_file.initTag()
                audio_file.tag.title = song_download.song.name
                audio_file.tag.artist = song_download.song.artist.name
                audio_file.tag.album = song_download.song.album.name
                audio_file.tag.save()
            logger.info(u'Saved: {}'.format(file_path))
        except DownloadError as e:
            logger.warning(e)
        except CorruptError as e:
            logger.warning(e)
    logger.debug('Got sentinel signal.')


class DownloadManager(object):
    """
    DownloadManager

    Creates a session that is reused by all workers.
    """
    downloads = Queue()
    session = requests.session()

    def __init__(self, workers=2):
        self.workers = [
            Process(
                name='Downloader-{}'.format(i),
                target=handle_downloads,
                args=(self.downloads, self.session)
            )
            for i in range(workers)
        ]

    def download(self, song_download, file_path):
        self.downloads.put((song_download, file_path))

    def start(self):
        for worker in self.workers:
            worker.daemon = True
            worker.start()

    def stop(self):
        logger.debug('DownloadManager wants to stop. Sending sentinel signal to Downloaders.')
        for worker in self.workers:
            self.downloads.put(SENTINEL)
        for worker in self.workers:
            if worker.is_alive():
                worker.join(timeout=30)

    def __del__(self):
        self.stop()

        # Cleanup the mess if things aren't stopped...
        for worker in self.workers:
            if worker.is_alive():
                worker.terminate()
