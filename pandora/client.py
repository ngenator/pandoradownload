import logging
import os
import time

from . import settings
from .api import APIClient, FILE_EXTENSIONS
from .downloader import DownloadManager, DownloadError
from .models import Song, SongDownload


class Pandora(object):
    logger = logging.getLogger('%s.Pandora' % __name__)

    def __init__(self, partner):
        """

        :param partner:
        :type partner:
        :return:
        :rtype:
        """
        self.partner = partner
        self.username = None
        self.stations = {}
        self.downloader = DownloadManager(4)
        self.downloader.start()
        self.client = APIClient(partner)

    def connect(self, username, password):
        self.username = username
        self.client.partner_login()
        self.client.user_login(username, password)

    def get_stations(self):
        """
        Gets the user's stations

        :return:
        :rtype:
        """
        for data in self.client.get_stations():
            station = PandoraStation(self, data)
            if station.token not in self.stations:
                self.stations[station.token] = station
            yield station

    def get_station(self, token):
        """
        Gets a user's station by token

        :param station_token:
        :type station_token:
        :return:
        :rtype:
        """
        if token not in self.stations:
            self.stations[token] = PandoraStation(self, self.client.get_station(token))
        return self.stations[token]

    def __repr__(self):
        return u"<Pandora: username={}>".format(self.username)


class PandoraStation(object):
    logger = logging.getLogger('%s.PandoraStation' % __name__)

    def __init__(self, pandora, data):
        self.pandora = pandora

        self.id = data.get('stationId', None)
        self.token = data.get('stationToken', None)
        self.name = data.get('stationName', None)
        self.genre = data.get('genre', [])

        self.songs = {}

        self._data = data

    def get_songs(self):
        """
        Gets the station's songs

        :return:
        :rtype:
        """
        for data in self.pandora.client.get_playlist(self.token):
            if 'adToken' in data:
                self.logger.debug('Skipping ad: %s', data)
                continue
            song = Song.get_or_create_from_json(data)
            if song.identity not in self.songs:
                self.songs[song.identity] = song
            yield song

    def download(
        self,
        to=settings.DEFAULT_DOWNLOAD_PATH,
        quality=settings.DEFAULT_AUDIO_QUALITY,
        quantity=10,
        only_liked=True
    ):
        """
        Downloads the station's songs

        :param to:
        :type to:
        :param quantity:
        :type quantity:
        :param only_liked_songs:
        :type only_liked_songs:
        :return:
        :rtype:
        """
        # Make sure directory exists
        if not os.path.isdir(to):
            os.mkdir(to)
        self.logger.info('Downloading {} songs from {} to {}'.format(quantity, self, to))

        count = 0
        downloading = True
        while downloading:
            for song in self.get_songs():
                if only_liked and not song.liked:
                    self.logger.info(u'Skipping: Song not liked %s', song)
                    break
                if song.downloaded:
                    self.logger.info(u'Skipping: Song already downloaded %s', song)
                    break
                try:
                    song_download = song.downloads.where(SongDownload.quality == quality).get()
                    file_name = u'{}{}'.format(
                        song.name,
                        FILE_EXTENSIONS.get(song_download.encoding, '.unknown'))
                    file_path = os.path.join(to, file_name)
                    self.pandora.downloader.download(song_download, file_path)
                    count += 1
                except SongDownload.DoesNotExist:
                    self.logger.error(u'Skipping: Missing download data for %s', song)
                if count >= quantity:
                    downloading = False
            self.logger.debug('Waiting...')
            time.sleep(25)

        self.logger.info('Downloaded %s songs', count)
        self.pandora.downloader.stop()
        self.logger.debug('Done.')

    def __repr__(self):
        return u"<Station: {}>".format(self.name)
