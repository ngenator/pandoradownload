import datetime
import logging

from peewee import IntegrityError

# from playhouse.apsw_ext import (
#     APSWDatabase as Database,
#     Model,
#     CharField,
#     DateTimeField,
#     BooleanField,
#     ForeignKeyField
# )
from peewee import (
    SqliteDatabase as Database,
    Model,
    CharField,
    DateTimeField,
    BooleanField,
    ForeignKeyField
)

database = Database('db.sqlite')
# database.get_conn().setbusytimeout(5000)


class Base(Model):
    logger = logging.getLogger(__name__)

    added = DateTimeField(default=datetime.datetime.now)
    last_seen = DateTimeField(default=datetime.datetime.now)

    def save(self, force_insert=False, only=None):
        self.last_seen = datetime.datetime.now()
        super(Base, self).save(force_insert, only)

    def seen(self):
        self.save()

    class Meta:
        database = database


class Artist(Base):
    """
    Artist Info
    """
    name = CharField(unique=True, index=True)

    detail_url = CharField()
    explorer_url = CharField()

    def __unicode__(self):
        return u"<Artist: '{}'>".format(self.name)

    def __repr__(self):
        return unicode(self)

    @classmethod
    def get_or_create_from_json(cls, data):
        try:
            artist = cls.select().where(cls.name == data['artistName']).get()
            artist.seen()
            cls.logger.debug('Found in database: %s', artist)
        except cls.DoesNotExist:
            artist = cls.create(
                name=data['artistName'],
                detail_url=data['artistDetailUrl'],
                explorer_url=data['artistExplorerUrl']
            )
            cls.logger.debug('Added to database: %s', artist)
        return artist


class Album(Base):
    """
    Album Info
    """
    identity = CharField(unique=True, index=True)
    name = CharField(index=True)
    artist = ForeignKeyField(Artist, related_name='albums')
    art_url = CharField()

    detail_url = CharField()
    explorer_url = CharField()
    amazon_url = CharField(null=True)

    amazon_digital_asin = CharField(null=True)

    def __unicode__(self):
        return u"<Album: '{}' by {}>".format(self.name, self.artist)

    def __repr__(self):
        return unicode(self)

    @classmethod
    def get_or_create_from_json(cls, data, artist=None):
        try:
            album = cls.select().where(cls.identity == data['albumIdentity']).get()
            album.seen()
            cls.logger.debug('Found in database: %s', album)
        except cls.DoesNotExist:
            artist = artist or Artist.get_or_create_from_json(data)

            album = cls.create(
                identity=data['albumIdentity'],
                name=data['albumName'],
                art_url=data['albumArtUrl'],
                amazon_url=data.get('amazonAlbumUrl'),
                detail_url=data['albumDetailUrl'],
                explorer_url=data['albumExplorerUrl'],
                amazon_digital_asin=data.get('amazonAlbumDigitalAsin'),
                artist=artist
            )
            cls.logger.debug('Added to database: %s', album)
        return album


class Song(Base):
    """
    Song Info
    """
    identity = CharField(unique=True, index=True)
    name = CharField(index=True)
    artist = ForeignKeyField(Artist, related_name='songs')
    album = ForeignKeyField(Album, related_name='songs')

    detail_url = CharField()
    explorer_url = CharField()
    itunes_url = CharField(null=True)

    amazon_digital_asin = CharField(null=True)

    liked = BooleanField()
    feedback_allowed = BooleanField()
    downloaded = BooleanField(default=False)

    def __unicode__(self):
        return u"<Song: '{}' by {}>".format(self.name, self.artist)

    def __repr__(self):
        return unicode(self)

    @classmethod
    def _clean_song_name(cls, song_name):
        song_name = song_name.replace('/', '')
        song_name = song_name.replace('\\', '')
        return song_name

    @classmethod
    def get_or_create_from_json(cls, data, artist=None, album=None):
        try:
            song = cls.select().where(cls.identity == data['songIdentity']).get()
            if song.liked != int(data['songRating']):
                song.liked = int(data['songRating'])
            song.seen()
            cls.logger.debug('Found in database: %s', song)
        except cls.DoesNotExist:
            song_name = cls._clean_song_name(data['songName'])

            artist = artist or Artist.get_or_create_from_json(data)

            album = album or Album.get_or_create_from_json(data, artist)

            song = cls.create(
                identity=data['songIdentity'],
                name=song_name,
                artist=artist,
                album=album,
                explorer_url=data['songExplorerUrl'],
                itunes_url=data.get('itunesSongUrl'),
                detail_url=data['songDetailUrl'],
                amazon_digital_asin=data.get('amazonSongDigitalAsin'),
                liked=int(data['songRating']),
                feedback_allowed=data['allowFeedback'],
            )

            cls.logger.info('Added to database: %s', song)
        for quality, audio_data in data['audioUrlMap'].iteritems():
            try:
                SongDownload.create(
                    song=song,
                    quality=quality,
                    encoding=audio_data['encoding'],
                    url=audio_data['audioUrl']
                )
            except IntegrityError:
                cls.logger.info("Already have db entry for %s with quality: %s", song, quality)

        return song


class SongDownload(Base):
    song = ForeignKeyField(Song, related_name='downloads')
    quality = CharField(index=True)
    encoding = CharField()
    url = CharField()

    class Meta:
        indexes = (
            (('song', 'quality'), True),
        )

    def __repr__(self):
        return "<Download: '{}' for {}>".format(self.quality, self.song)


def create():
    Artist.create_table(True)
    Album.create_table(True)
    Song.create_table(True)
    SongDownload.create_table(True)


create()
