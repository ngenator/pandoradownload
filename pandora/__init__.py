import logging.config

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s\t[%(levelname)s]\t %(processName)s \t%(name)s:%(funcName)s - %(message)s'
        },
        'simple': {
            'format': '[%(levelname)s] \t%(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'pandora': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False
        }
    }
})
