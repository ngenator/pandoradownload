CONFIG_FILE = 'credentials.cfg'

PARTNERS = {
    'android': {
        'api': {
            'host': 'tuner.pandora.com',
            'path': '/services/json/?',
        },
        'encryption': {
            'encrypt-key': '6#26FRL$ZWD',
            'decrypt-key': 'R=U!LH$O2B#',
        },
        'partner-data': {
            'deviceModel': 'android-generic',
            'username': 'android',
            'password': 'AC7IBG09A3DTSYM4R41UJWL07VLN8JI7',
            'version': '5',
        }
    },
    'iphone': {
        'api': {
            'host': 'tuner.pandora.com',
            'path': '/services/json/?',
        },
        'encryption': {
            'encrypt-key': '721^26xE22776',
            'decrypt-key': '20zE1E47BE57$51',
        },
        'partner-data': {
            'deviceModel': 'IP01',
            'username': 'iphone',
            'password': 'P2E4FC0EAD3*878N92B2CDp34I0B1@388137C',
            'version': '5',
        }
    },
    'windows-mobile': {
        'api': {
            'host': 'tuner.pandora.com',
            'path': '/services/json/?',
        },
        'encryption': {
            'encrypt-key': '',
            'decrypt-key': '',
        },
        'partner-data': {
            'deviceModel': 'VERIZON_MOTOQ9C',
            'username': 'winmo',
            'password': 'ED227E10a628EB0E8Pm825Dw7114AC39',
            'version': '5',
        }
    },
    'pandora-one':{
        'api': {
            'host': 'internal-tuner.pandora.com',
            'path': '/services/json/?',
        },
        'encryption': {
            'encrypt-key': '2%3WCL*JU$MP]4',
            'decrypt-key': 'U#IO$RZPAB%VX2',
        },
        'partner-data': {
            'deviceModel': 'D01',
            'username': 'pandora one',
            'password': 'TVCKIBGS9AO9TSYLNNFUML0743LH82D',
            'version': '5',
        }
    }
}
DEFAULT_PARTNER = 'android'

DEFAULT_AUDIO_QUALITY = 'highQuality'

DEFAULT_DOWNLOAD_PATH = 'music'