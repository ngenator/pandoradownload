import ConfigParser
import functools
import json
import logging
import os
import requests
import time
import urllib

from .encryption import PandoraEncryption
from . import settings

API_ERROR_UNKNOWN = 0
API_ERROR_MAINTENANCE_MODE = 1
API_ERROR_URL_PARAM_MISSING_METHOD = 2
API_ERROR_URL_PARAM_MISSING_AUTH_TOKEN = 3
API_ERROR_URL_PARAM_MISSING_PARTNER_ID = 4
API_ERROR_URL_PARAM_MISSING_USER_ID = 5
API_ERROR_SECURE_PROTOCOL_REQUIRED = 6
API_ERROR_CERTIFICATE_REQUIRED = 7
API_ERROR_PARAMETER_TYPE_MISMATCH = 8
API_ERROR_PARAMETER_MISSING = 9
API_ERROR_PARAMETER_VALUE_INVALID = 10
API_ERROR_API_VERSION_NOT_SUPPORTED = 11
API_ERROR_COUNTRY_NOT_SUPPORTED = 12
API_ERROR_INSUFFICIENT_CONNECTIVITY = 13
API_ERROR_METHOD_INVALID = 14
API_ERROR_READ_ONLY_MODE = 1000
API_ERROR_INVALID_AUTH_TOKEN = 1001
API_ERROR_INVALID_LOGIN = 1002
API_ERROR_LISTENER_NOT_AUTHORIZED = 1003
API_ERROR_USER_NOT_AUTHORIZED = 1004
API_ERROR_PARTNER_NOT_AUTHORIZED = 1010
API_ERROR_INVALID_USERNAME = 1011
API_ERROR_INVALID_PASSWORD = 1012
API_ERROR_USERNAME_ALREADY_EXISTS = 1013
API_ERROR_PLAYLIST_EXCEEDED = 1039

API_ERROR_LOOKUP = {
    API_ERROR_UNKNOWN: 'API_ERROR_UNKNOWN',
    API_ERROR_MAINTENANCE_MODE: 'API_ERROR_MAINTENANCE_MODE',
    API_ERROR_URL_PARAM_MISSING_METHOD: 'API_ERROR_URL_PARAM_MISSING_METHOD',
    API_ERROR_URL_PARAM_MISSING_AUTH_TOKEN: 'API_ERROR_URL_PARAM_MISSING_AUTH_TOKEN',
    API_ERROR_URL_PARAM_MISSING_PARTNER_ID: 'API_ERROR_URL_PARAM_MISSING_PARTNER_ID',
    API_ERROR_URL_PARAM_MISSING_USER_ID: 'API_ERROR_URL_PARAM_MISSING_USER_ID',
    API_ERROR_SECURE_PROTOCOL_REQUIRED: 'API_ERROR_SECURE_PROTOCOL_REQUIRED',
    API_ERROR_CERTIFICATE_REQUIRED: 'API_ERROR_CERTIFICATE_REQUIRED',
    API_ERROR_PARAMETER_TYPE_MISMATCH: 'API_ERROR_PARAMETER_TYPE_MISMATCH',
    API_ERROR_PARAMETER_MISSING: 'API_ERROR_PARAMETER_MISSING',
    API_ERROR_PARAMETER_VALUE_INVALID: 'API_ERROR_PARAMETER_VALUE_INVALID',
    API_ERROR_API_VERSION_NOT_SUPPORTED: 'API_ERROR_API_VERSION_NOT_SUPPORTED',
    API_ERROR_COUNTRY_NOT_SUPPORTED: 'API_ERROR_COUNTRY_NOT_SUPPORTED',
    API_ERROR_INSUFFICIENT_CONNECTIVITY: 'API_ERROR_INSUFFICIENT_CONNECTIVITY',
    API_ERROR_METHOD_INVALID: 'API_ERROR_METHOD_INVALID',
    API_ERROR_READ_ONLY_MODE: 'API_ERROR_READ_ONLY_MODE',
    API_ERROR_INVALID_AUTH_TOKEN: 'API_ERROR_INVALID_AUTH_TOKEN',
    API_ERROR_INVALID_LOGIN: 'API_ERROR_INVALID_LOGIN',
    API_ERROR_LISTENER_NOT_AUTHORIZED: 'API_ERROR_LISTENER_NOT_AUTHORIZED',
    API_ERROR_USER_NOT_AUTHORIZED: 'API_ERROR_USER_NOT_AUTHORIZED',
    API_ERROR_PARTNER_NOT_AUTHORIZED: 'API_ERROR_PARTNER_NOT_AUTHORIZED',
    API_ERROR_INVALID_USERNAME: 'API_ERROR_INVALID_USERNAME',
    API_ERROR_INVALID_PASSWORD: 'API_ERROR_INVALID_PASSWORD',
    API_ERROR_USERNAME_ALREADY_EXISTS: 'API_ERROR_USERNAME_ALREADY_EXISTS',
    API_ERROR_PLAYLIST_EXCEEDED: 'API_ERROR_PLAYLIST_EXCEEDED',
}

FILE_EXTENSIONS = {
    'mp3': '.mp3',
    'aacplus': '.mp4',
}


class ApiRequestError(Exception):
    pass


class ApiError(Exception):
    pass


class AuthenticationError(ApiError):
    pass


class PartnerNotAuthorized(AuthenticationError):
    pass


class UserNotAuthorized(AuthenticationError):
    pass


def requires_partner_authentication(func):
    @functools.wraps(func)
    def wrapper(self, *args, **kwargs):
        if not self.is_partner_authenticated:
            raise PartnerNotAuthorized(
                'The API client is not yet partner authenticated. Did you call `partner_login()`?')
        return func(self, *args, **kwargs)

    return wrapper


def requires_authentication(func):
    @functools.wraps(func)
    def wrapper(self, *args, **kwargs):
        if not self.is_authenticated:
            raise UserNotAuthorized('The API client is not yet user authenticated. Did you call `user_login()`?')
        return func(self, *args, **kwargs)

    return wrapper


class APIClient(object):
    """
    Pandora Api Client for Json Api v5

    http://6xq.net/playground/pandora-apidoc/
    """
    logger = logging.getLogger('%s.APIClient' % __name__)

    user_id = None
    user_auth_token = None
    partner_id = None
    partner_auth_token = None
    time_offset = None

    def __init__(self, partner):
        """

        :param partner: Partner data from settings
        :type partner: dict
        :return:
        :rtype:
        """
        self.partner = partner

        self.session = requests.session()
        self.session.headers.update({
            'User-Agent': 'Pandora',
            'Content-Type': 'text/plain',
        })

        self.config = ConfigParser.RawConfigParser()
        if os.path.exists(settings.CONFIG_FILE):
            self.config.read(settings.CONFIG_FILE)

        self.encryption = PandoraEncryption(
            partner['encryption']['encrypt-key'],
            partner['encryption']['decrypt-key']
        )

    def api_call(self, method, args=None, https=False, blowfish=True):
        """
        Makes an API call to Pandora

        :param method:
        :type method:
        :param args:
        :type args:
        :param https:
        :type https:
        :param blowfish:
        :type blowfish:
        :return:
        :rtype:
        """
        # Setup the url params
        params = {'method': method}

        if self.user_id is not None:
            params['user_id'] = self.user_id
        if self.partner_id is not None:
            params['partner_id'] = self.partner_id

        if self.user_auth_token is not None:
            params['auth_token'] = urllib.quote_plus(self.user_auth_token)
        elif self.partner_auth_token is not None:
            params['auth_token'] = urllib.quote_plus(self.partner_auth_token)

        # Setup the post data
        data = args or {}

        if self.time_offset is not None:
            data['syncTime'] = int(time.time() + self.time_offset)
        if self.user_auth_token is not None:
            data['userAuthToken'] = self.user_auth_token
        elif self.partner_auth_token is not None:
            data['partnerAuthToken'] = self.partner_auth_token

        self.logger.debug('url params: %s', params)
        self.logger.debug('post data: %s', data)

        url = '{protocol}://{host}{path}{params}'.format(
            protocol='https' if https else 'http',
            host=self.partner['api']['host'],
            path=self.partner['api']['path'],
            params='&'.join('{}={}'.format(k, v) for k, v in params.items())
        )
        self.logger.debug('url: %s', url)

        data = json.dumps(data)
        response = self.session.post(url, data=self.encryption.encrypt(data) if blowfish else data)

        return self.handle_response(response)

    def handle_response(self, response):
        """
        Handles API response

        :param response:
        :type response:
        :return:
        :rtype:
        """
        data = response.json()
        self.logger.debug('content: %s', response.content)
        if data.get('stat', 'fail') == 'fail':
            code = data.get('code', API_ERROR_UNKNOWN)

            # If the auth token is invalid, remove it from the config file
            if code == API_ERROR_INVALID_AUTH_TOKEN:
                if self.config.has_section(self.username):
                    self.config.remove_section(self.username)
                    with open(settings.CONFIG_FILE, 'wb') as f:
                        self.config.write(f)

            raise ApiError(API_ERROR_LOOKUP[code])
        return data.get('result', True)


    @property
    def is_authenticated(self):
        """
        Checks if the ApiClient is fully authenticated

        :return: True if fully authenticated
        :rtype: bool
        """
        return self.is_partner_authenticated and self.is_user_authenticated

    @property
    def is_partner_authenticated(self):
        """
        Checks if the ApiClient is partner authenticated

        :return: True if partner authenticated
        :rtype: bool
        """
        return self.partner_id is not None and self.partner_auth_token is not None

    @property
    def is_user_authenticated(self):
        """
        Checks if the ApiClient is user authenitcated

        :return: True if user authenticated
        :rtype: bool
        """
        return self.user_id is not None and self.user_auth_token is not None

    def partner_login(self):
        """
        Calls `auth.partnerLogin`

        :return:
        :rtype:
        """
        partner = self.api_call(
            'auth.partnerLogin',
            self.partner['partner-data'],
            https=True,
            blowfish=False
        )
        self.partner_id = partner['partnerId']
        self.partner_auth_token = partner['partnerAuthToken']
        self.time_offset = int(self.encryption.decrypt(partner['syncTime'])[4:14]) - time.time()
        self.logger.debug(partner)
        return partner

    @requires_partner_authentication
    def user_login(self, username, password):
        """
        Calls `auth.userLogin`

        :param username: Pandora username
        :type username: str
        :param password: Pandora password
        :type password: str
        :return:
        :rtype: dict
        """
        self.username = username
        if self.config.has_section(username):
            self.logger.debug('using existing credentials for %s', username)
            self.user_id = self.config.get(username, 'user_id')
            self.user_auth_token = self.config.get(username, 'user_auth_token')
            return {self.user_id, self.user_auth_token}

        self.logger.debug('logging in as %s', username)
        user = self.api_call(
            'auth.userLogin',
            {
                'loginType': 'user',
                'username': username,
                'password': password
            },
            https=True)
        self.user_id = user['userId']
        self.user_auth_token = user['userAuthToken']
        self.config.add_section(username)
        self.config.set(username, 'user_id', self.user_id)
        self.config.set(username, 'user_auth_token', self.user_auth_token)
        with open(settings.CONFIG_FILE, 'wb') as f:
            self.config.write(f)
        return user


    @requires_authentication
    def get_stations(self):
        """
        Calls `user.getStationList`

        :return:
        :rtype:
        """
        return self.api_call('user.getStationList')['stations']

    @requires_authentication
    def get_station(self, station_token):
        """
        Calls `station.getStation`

        :param station_token:
        :type station_token:
        :return:
        :rtype:
        """
        return self.api_call('station.getStation', {'stationToken': station_token})

    @requires_authentication
    def get_playlist(self, station_token):
        """
        Calls `station.getPlaylist`

        :param station_token:
        :type station_token:
        :return:
        :rtype:
        """
        return self.api_call('station.getPlaylist', {'stationToken': station_token}, https=True)['items']