import logging

from .blowfish import Blowfish


class PandoraEncryption(object):
    logger = logging.getLogger('%s.PandoraEncryption' % __name__)

    def __init__(self, encrypt_key, decrypt_key):
        self._encoder = Blowfish(encrypt_key)
        self._decoder = Blowfish(decrypt_key)

    def _pad(self, data, length):
        return data + "\0" * (length - len(data))

    def encrypt(self, data):
        self.logger.debug('data: %s', data)
        encrypted = ''.join(
            self._encoder.encrypt(self._pad(data[i:i + 8], 8)).encode('hex') for i in xrange(0, len(data), 8))
        self.logger.debug('encrypted: %s', encrypted)
        return encrypted

    def decrypt(self, data):
        self.logger.debug('data: %s', data)
        decrypted = ''.join(
            self._decoder.decrypt(self._pad(data[i:i + 16].decode('hex'), 8)) for i in xrange(0, len(data), 16)).rstrip(
            '\x08')
        self.logger.debug('decrypted: %s', decrypted)
        return decrypted