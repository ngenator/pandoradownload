from pandora.client import Pandora
from pandora.settings import PARTNERS

if __name__ == "__main__":
    # Setup the pandora client
    # If the partner is `pandora-one`, a Pandora One account is required
    p = Pandora(PARTNERS['pandora-one'])
    # Connect to Pandora using your email and password
    p.connect('email', 'password')
    # Get a list of all your stations, along with their tokens
    for s in p.get_stations():
        print s, s.token
    # Get one specific station by its token
    station = p.get_station('1065124393273653164')
    # Download 8 songs from the station, ignoring whether you liked them or not
    station.download(quantity=8, only_liked_songs=False)